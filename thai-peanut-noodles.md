% Thai Peanut Noodles

Adapted from
https://www.crunchycreamysweet.com/thai-peanut-noodles/#wprm-recipe-container-21050
- one of my wife's go-to comfort foods.



## Sauce Ingredients

1/4 cup smooth peanut butter (or 3/4 cup if doubling)
1/8 cup low-sodium soy sauce
1/8 cup soy sauce
1 teaspoon sesame oil
2 tablespoons packed brown sugar or honey
1 tbsp shaoxing wine
1 tbsp mirin
1/4 tsp chili paste (e.g. sambal oelek)
3 tablespoons water

## Noodles

8 oz noodles (check with Steph on type)
Scrambled eggs (or other protein)
2 cloves garlic
julienned carrots
