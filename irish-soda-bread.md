% Irish Soda Bread

https://sallysbakingaddiction.com/grandmas-irish-soda-bread/ - best soda bread
I've found.

Hard to go wrong with any of Sally's recipes, really.


## Ingredients

* 1 3/4 cups (420 ml) buttermilk
* 1 large egg
* 4 1/4 cups (531 g) all-purpose flour
* 3 Tablespoons granulated sugar
* 1 teaspoon baking soda
* 1 teaspoon salt
* 5 tablespoons unsalted butter, cold and cubed

## Preparation

Preheat overn to 400° F.

Line a baking sheet with parchment paper or a silicone baking mat. Alternately,
cook in an iron skillet or dutch oven (do not put lid on).

Whisk buttermilk and egg together. Set aside.

Whisk flour, granulated sugar, baking soda, and salt together in a large bowl.

Cut in the butter with a pastry cutter, a fork, or your fingers, until you have
pea-sized crumbs. (TODO Try freezing/grating butter technique?)

Pour in the buttermilk/egg mixture.

Gently fold together until dough is too stiff to stir.

Pour crumbly dough onto a lightly-floured work surface, and with floured
hands, knead until all flour is moistened (~30 seconds).

Transfer the dough to the baking sheet.

Score the top with an X about 1/2 inch deep.

Bake until bread is golden-brown and center appears cooked through, ~45 - 55
minutes. Tent with aluminum foil if there is heavy browning on top. Center of
bread should read 195° F.

Allow to cool ten minutes before transferring to wire rack.
