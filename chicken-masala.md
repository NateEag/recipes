% Slow-Cooker Chicken Masala

Adapted from
https://www.thekitchn.com/recipe-slow-cooker-chicken-tikka-masala-recipes-from-the-kitchn-211284.


## Ingredients

2 pounds cubed chicken (thighs or breasts)
1/2 cup plain yogurt
1 tsp salt

### Sauce

1/2 tsp salt
1 large yellow onion, diced
2 tablespoons oil
3 cloves minced garlic
1 tablespoon peeled and minced fresh ginger
2 teaspoons ground coriander
2 teaspoons garam masala
1 teaspoon ground cumin
1 teaspoon ground turmeric
2 tablespoons tomato paste
1 15 oz can diced tomatoes, drained
3/4 cup heavy cream or coconut milk


## Instructions

Add chicken, yogurt, and 1 tsp of salt to slow cooker and stir to combine.

Prepare the sauce as below.

Cover and cook on High for 4 hours (3 on our cooker; it's hot) or on Low for 8
hours.

Stir in coconut milk. If you want a thicker sauce, take the lid off and cook on
High for ~30 minutes.

### Sauce

Heat oil in a large frying pan until shimmering.

Add the onion and cook until tender, about 8 minutes.

Add garlic, ginger, coriander, garam masala, cumin, and turmeric. Cook until
fragrant, about 1 minute.

Add drained tomatoes and 1/2 tsp of salt. Bring to a simmer, scraping up
browned bits from bottom of the pan.

Add to slow cooker and stir to combine with chicken.
