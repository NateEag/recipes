% Beef With Broccoli

Adapted from
https://thewoksoflife.com/beef-with-broccoli-all-purpose-stir-fry-sauce/#recipe.

## Ingredients

### Beef Marinade

1 pound flank steak
1/4 tsp baking soda
3 tbsp water
1 1/2 tsp cornstarch
2 tsp vegetable oil
1 tsp oyster sauce

### Sauce

2/3 cup low sodium chicken stock
1 1/2 tsp granulated sugar
1 1/2 tbsp soy sauce
1 tsp dark soy sauce
1 tbsp oyster sauce
1/2 tsp sesame oil
1/8 tsp white pepper

### Vegetables

4 cups broccoli florets
3 tbsp vegetable oil
2 cloves garlic
1/4 tsp ginger
1 tbsp Shaoxing wine
2 1/2 tbsp cornstarch
3 tbsp water


## Instructions

### Beef Marinade

Add sliced beef, baking soda, and water to a bowl. Massage beef with hands
until all liquid is absorbed. Mix in cornstarch, oil, and oyster sauce.

Put in fridge to marinate for at least 30 minutes.

### Sauce

Mix together:

* chicken stock
* sugar
* soy sauce
* dark soy sauce
* oyster sauce
* sesame oil
* white pepper

### Cook

Bring a pot of water to a boil and blanch broccoli for 30 - 60 seconds.

Heat wok over high heat until smoking. Add vegetable oil and sear beef until
browned (2 - 3 minutes). Turn off heat, remove beef from wok, and set aside.

Put wok over medium heat and add a tablespoon of oil, plus garlic and ginger.
Stir for a few seconds then pour the Shaoxing wine around the wok's perimeter.

Add the sauce mixture and stir to deglaze (be sure to get the browned bits from
stir-frying the beef)

Bring sauce to a simmer.

Stir cornstarch and water into a well-combined slurry, then drizzle the mixture
into sauce while stirring constantly. Simmer and thicken for 20 seconds.

Add the blanched broccoli and seared beef, along with any juices. Mix
everything over medium heat until broccoli and beef are well-coated.

If sauce seems thin, turn up the heat to reduce. If it seems thick, add a
splash of chicken stock or water.
