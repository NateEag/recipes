% Banana Bread

Lightly adapted from [Sally's Baking
Addiction](https://sallysbakingaddiction.com/best-banana-bread-recipe/), whose
recipes are spectacularly delicious and well-documented.

Really, just added flax seeds for a little fiber, and lots of chocolate chips,
because somehow she thinks they're optional. o_O


## Ingredients

2 cups (250g) all-purpose flour
1 teaspoon baking soda
2 tablespoons flax seed (optional fiber boost)
1/4 teaspoon salt
1/2 teaspoon ground cinnamon
1/2 cup (1 stick or 115g) room temp unsalted butter
3/4 cup (150g) packed light or dark brown sugar
2 large eggs, at room temperature
1/3 cup (80g) room temp plain yogurt or sour cream
2 cups (460g) mashed bananas (~4 large ripe bananas)
1 tsp vanilla extract
1 1/2 cups chocolate chips


## Steps

Adjust oven rack to lower third position.

Preheat oven to 350 degrees Fahrenheit

Grease a 9x5 metal loaf pan (grease and flour if using nonstick bakeware). Set
aside.

Whisk flour, baking soda, salt, cinnamon, chocolate chips, and optional flax
seed together in a medium bowl.

Use a mixer with a paddle or whisk attachment to cream the butter with the
brown sugar.

Add the eggs one at a time with the mixer on medium, beating well after each
addition.

Add the yogurt, mashed bananas, and vanilla extract. Mix until combined.

With the mixer on low, slowly beat the dry ingredients into the wet
ingredients until no flour pockets remain. Do not overmix.

Pour into the pan and bake for 30 minutes. Tent loosely with aluminum foil and
bake for another 35 - 40 minutes.

Cool in pan on wire rack for one hour. Remove bread from pan and cool directly
on wire rack until ready to serve.
