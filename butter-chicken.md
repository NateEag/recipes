% Butter Chicken

Adapted from https://cafedelites.com/butter-chicken/.


## Ingredients

### Chicken Marinade

28 oz (800g) bite-sized chicken pieces (deboned/skinned breasts or thighs)
1/2 cup plain yogurt
1 1/2 tablespoons minced garlic
1 tablespoon ginger, minced or finely-grated
2 teaspoons garam masala
1 teaspoon turmeric
1 teaspoon ground cumin
1 teaspoon red chili powder
1 teaspoon salt

### Sauce

2 tablespoons olive oil
2 tablespoons ghee (or 1 tbs butter + 1 tbs oil)
1 large onion, sliced or chopped
1 1/2 tablespoons garlic, minced
1 tablespoon ginger, minced or finely grated
1 1/2 teaspoons ground cumin
1 1/2 teaspoons garam masala
1 teaspoon ground coriander
14 oz (400 g) crushed tomatoes
1 teaspoon red chili powder (adjust to taste)
1 1/4 teaspoons salt (adjust to taste)
1 cup heavy cream
1 tablespoon sugar
1/2 teaspoon kasoori methi (or dried fenugreek leaves) [optional]


## Instructions

Combine chicken with all of the ingredients for the chicken marinade. Marinate
for at least 30 minutes (overnight if time allows)

Heat oil in a large skillet or pot over medium-high heat. When sizzling, add
chicken pieces in batches of two or three, without crowding the pan. Fry on
each side until browned (about 3 minutes). Set aside and keep warm.

Heat butter or ghee in the same pan. Fry onions until they start sweating
(about 6 minutes) while scraping up any browned bits.

Add garlic and ginger and sauté until fragrant (about 1 minute). Add ground
coriander, cumin and garam masala. Let cook for about 20 seconds until
fragrant, while stirring occasionally.

Add crushed tomatoes, chili powder and salt. Let simmer for about 10-15
minutes, stirring occasionally until sauce thickens and becomes a deep brown
red colour.

Remove from heat and blend until smooth. Use immersion blender if available -
you may need to blend in batches for smaller blenders. You may also need to add
a couple tablespoons of water to help it blend (up to 1/4 cup). Return puréed
sauce to pan.

Stir the cream, sugar and optionally kasoori methi (or fenugreek leaves)
through the sauce.

Add chicken and juices back into the pan and cook until chicken is cooked
through and sauce is thick and bubbling (about 8-10 minutes).
