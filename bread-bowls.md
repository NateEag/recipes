% Bread Bowls

If you're not comfortable baking breads, pull up Sally's detailed recipe and follow it. I've just got the Cliffs Notes here since I've done this a lot.

https://sallysbakingaddiction.com/homemade-bread-bowls/

The basic bread recipe can also be used for rolls (yields ~24), pizza crust (~4 12-inch crusts), or crusty bread (3 rounds).

## Ingredients

4 1/2 teaspoons active dry yeast
2 1/4 cups (540ml) warm water (110°F – 115°F)
2 teaspoons granulated sugar
2 teaspoons salt
2 Tablespoons (30ml) olive oil
6 cups (780g) bread flour (spooned & leveled), plus more for hands and
surface*
1 large egg beaten with 1 Tablespoon water or milk


## Mix and Knead Dough

Pour the warm water over yeast in stand mixer bowl, fitted with a dough hook attachment. Whisk together and allow to sit for 5 minutes.

With the stand mixer running on low speed, add the sugar, salt, olive oil, 4 cups of bread flour, and seasonings (if using, see recipe note).

Beat on low speed for 1 minute, then add remaining 2 cups of flour.

Knead the dough: Keep the dough in the mixer and beat for an additional 5-6 full minutes, or knead by hand on a lightly floured surface for 5-6 full minutes, until the dough passes the windowpane test.


## First Rise

Let dough rise until doubled in size, ~90 minutes.


## Form Bread Bowls

Punch down dough and turn it out onto a lightly floured surface.

Cut into 6 even pieces and form each into a large ball.

## Bake

Preheat oven to 400°F.

Line two baking sheets with parchment paper or silicone baking mats. Place 3 dough balls on each. Cover lightly and rest for 20 minutes.

Brush each dough ball with egg wash and score an X into the top of each.

Bake for ~30 minutes, until golden brown. An instant-read thermometer should display 195° F at the center.

Let cool, then cut out the middles to make bowls. Save that middle for dipping in soup, though!
