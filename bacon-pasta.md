% Bacon Pasta

Yes, technically, this is "linguine carbonara."

Around these parts, we know why we really eat this stuff.

Adapted from https://www.foodandwine.com/recipes/linguine-carbonara by my wife.


## Ingredients

2 tablespoons olive oil
2 tablespoons butter
1/4 pound sliced bacon
2 cloves garlic
1/2 cup red wine
1/2 teaspoon fresh-ground black pepper
2 eggs
2/3 cup grated Parmesan cheese, plus more for serving
1/2 teaspoon salt
2 tablespoons kosher salt (for pasta water)
3/4 pound linguine
2 tablespoons chopped fresh parsley


## Steps

Fill a large pot with water. Add kosher salt. Put it over high heat and bring
to a boil.

Mince garlic.

Mince parsley.

If using a block of parmesan, grate it now.

Combine oil and butter in a small frying pan over medium heat.

Cut bacon crosswise into thin strips and add to butter / oil mixture. If you
have kitchen shears, it's easiest to hold two or three strips over the pan at
once, then cut so the slices drop into the pan.

Cook the bacon until brown but not crisp (about 4 minutes).

Add garlic, wine, and pepper.

Reduce until about 2 tablespoons of wine remain (about 3 minutes). Remove from
heat.

In a large bowl, combine eggs, cheese, and salt. Whisk thoroughly.

Add linguine to the pot of boiling salted water. Cook until al dente, about 12
minutes.

When the linguine has a minute or two remaining, add about half a ladle [FIXME
get a real unit here] of pasta water slowly into the egg mixture, whisking
vigorously the whole time. This helps prevent the eggs from scrambling when you
add the pasta.

When the pasta is finished cooking, drain it and add it to the egg mixture.
Toss quickly. Pour the bacon mixture over the linguine. Add parsley and toss
until just mixed.

Serve immediately.
