% Meatloaf

## Ingredients

1 to 1.5 pounds ground beef
2 eggs
1 cup oatmeal
1/4 tsp pepper
1 tsp salt
1/2 onion, diced
1 cup ketchup
1 tbsp steak sauce
1 tbsp thyme


## Instructions

Combine ingredients in large bowl by hand, mixing until consistency is even.

Pour into loaf pan.

Bake at 350 degrees Fahrenheit for 1.5 hours. Pour 3/4 cup of water on top of
loaf at 1 hour mark.


## Pairs Well With

Cook baked potatoes in the oven, starting them before you start prepping the
meatloaf. Begin at 400 and lower the temp to 350 when the meatloaf goes in.

Put a tray of veggies to roast in for the last twenty minutes or so.
