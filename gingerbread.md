% Gingerbread Cookies / Gingerbread House Dough

## Ingredients

* 3/4 cup (1 1/2 sticks) softened butter
* 3/4 cup packed dark brown sugar
* 1/2 cup dark molasses
* 1 tablespoon fresh lemon juice
* 4 1/2 cups all-purpose flour
* 1 1/2 teaspoons baking soda
* 1/2 teaspoon ground cloves
* 1 teaspoon ground cinnamon
* 2 1/2 teaspoons ground ginger
* 1/4 teaspoon ground cardamom
* 1/2 teaspoon ground nutmeg
* 1 teaspoon salt
* 3 tablespoons cold water


## Directions

Cream the butter and brown sugar together until fluffy.

Add the molasses and lemon juice. Beat again.

Combine all dry ingredients in a separate bowl.

Add dry ingredients to creamed butter and blend until crumbly.

Add cold water and mix until a dough is formed.

Knead a minute by hand so the dough holds together.

Wrap dough in plastic wrap and refrigerate 1.5 hours.

Roll the dough to 1/4" thickness and cut out gingerbread men.

Bake in 350 F oven for 15 to 20 minutes, until edges of cookies begin to turn
brown.
