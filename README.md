% Family Recipes

I cook sometimes.

I also like to bake.

I intend to put recipes I use regularly here.

I may also flirt with getting enough structure around recipes to make them
useful with other tools. [Cooklang](https://cooklang.org/) misses the mark for
me, as does [RecipeMD](https://recipemd.org/specification.html) -
[recipe-flavored
Markdown](https://flutterawesome.com/recipe-flavored-markdown-make-recipes-easy-to-create-and-maintain/)
is closer to what I'm looking for.

The thing I most want is an elegant way to present a default ordering of steps
that shows the true relationship of steps, as well as times between steps, so
it's easy to extract things like "steps that can be done early" or "steps in
different recipes that can be interleaved". The phrase "While the {ingredient}
cooks" is a good example of a hint that a step can be interleaved.

...on reviewing this, it strikes me that a simple way to represent independent
steps in the process could be as simple as ### headers. Any set of steps that
necessarily go together are grouped together under a single header.

Also interesting is putting step time estimates in parentheses, as follows:
"...cook until fragrant (about 1 minute)." It should be feasible to parse
parentheticals for `${numeral} ${unit_of_time}` and attach those to the
described step.


## Recipes To Add

https://www.tumblr.com/indelibleevidence/724656401625088000/the-sad-bastard-cookbook-by-traum-books
- a cookbook for people who have major life struggles. Mine it for simple
recipes that'll work when everyone's exhausted and shot?
