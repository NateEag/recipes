% Broccoli Cheddar Soup

This really is a bang-on imitation of Panera's broccoli cheddar.

https://www.averiecooks.com/best-broccoli-cheese-soup-better-panera-copycat/#h-broccoli-cheese-soup-ingredients

Pairs beautifully with some homemade bread bowls.

## Ingredients

1 tablespoon + 4 tablespoons unsalted butter, divided
1 small or medium sweet yellow onion, diced small
1 clove garlic, peeled and minced finely
1/4 cup all-purpose flour
2 cups low-sodium vegetable stock (chicken stock may be substituted)
2 cups half-and-half
2 to 3 cups broccoli florets diced into bite-size pieces
1 cup broccoli stems, diced into bite-size pieces (optional)
2 large carrots, trimmed, peeled, and sliced into very thin rounds or julienned
3/4 teaspoon salt
3/4 teaspoon freshly ground black pepper
1/2 teaspoon smoked (or regular) paprika
1/2 teaspoon dry mustard powder
1 pinch cayenne pepper, optional and to taste
8 ounces grated extra-sharp cheddar cheese (buy an expensive one for the flavor boost, and don't try pre-grated, it won't melt well)


## Chop Broccoli and Carrots

Chop the broccoli very fine, including stems.


## Sauté Onions and Garlic

Add 1 tablespoon butter and diced onion to bowl and saute until barely browned
(about 4 minutes). Stir intermittently.

Add garlic and saute an additional 30 seconds.

Remove from heat and set aside.


## Make Soup Base

In a large heavy-bottom pot, add 4 tablespoons butter and the flour, and cook over medium heat for about 3 to 5 minutes to make a roux, whisking constantly, until flour is thickened.

Slowly add the vegetable stock, whisking constantly.

Slowly add the half-and-half, whisking constantly.

Allow mixture to simmer over low heat for about 15 to 20 minutes, or until it has reduced and thickened some. Whisk intermittently to re-incorporate the ‘skin’ that inevitably forms, this is normal.


## Add Veggies And Cook Down

Add the broccoli, carrots, and the onion and garlic you previously set aside.

Add all seasonings (salt, pepper, paprika, dry mustard powder, and cayenne).

Stir to combine.

Simmer over low heart for 20 to 25 minutes.


## Add Cheese

Add the grated cheese. Stir to combine.

Serve immediately.
