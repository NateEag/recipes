% Williams-Sonoma Beef and Red Wine Stew

## Ingredients

2 slices thick-cut bacon, sliced
2 lbs boneless beef chuck
1 1/2 tsp salt
1/4 tsp pepper
2 - 3 tbsp extra virgin olive oil
1 bay leaf
1 tsp fresh thyme leaves
1 cup barley
1 yellow onion, diced
2 - 4 carrots
2 - 4 parsnips
1 clove garlic, minced
1 1/2 cups red wine
3 cups chicken stock


## Steps

Brown bacon in olive oil

Brown beef in olive oil (in batches; browning will not happen if it more than covers bottom of pot)

Re-add bacon to pot and return all beef to it. Add onion and garlic.
Immediately deglaze pan with wine.

Add all other ingredients. Braise in 350 Fahrenheit oven for 1 hour. Add
carrots and parsnips and braise for an additional 20 minutes.


## Modifications

Halve the chicken stock if not using barley.
